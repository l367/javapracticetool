//DEPS com.github.freva:ascii-table:1.8.0
import com.github.freva.asciitable.*;
import java.io.*;
import java.util.*;
class test{
	public static void main(String [] args) throws Exception{
	boolean [][] cases=new boolean[][] {
	    {false, false},{false, true}, {true, false}, {true, true}
	};
	String[] headers = {"p", "q", "!(p && q)", "!p || !q"};
	List<String[]> rows=new ArrayList<>();
	for (int i=0; i<cases.length; i++) {
		rows.add(new String[] {""+cases[i][0], ""+cases[i][1], ""+(!(cases[i][0] && cases[i][1])), ""+(!cases[i][0] || !cases[i][1])   });
	}
	String [][] data=rows.toArray(new String[0][0]);
	System.out.println(AsciiTable.getTable(headers, data));
	test();
	}
	static public int test(){
	    return 1;
	}

}