import java.util.*;
class TreeNode {
	private double value = -1;
	private List<TreeNode> childNodes=new ArrayList<>();

	public TreeNode(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public TreeNode getLeftChild() {
		return this.leftChild;
	}

	public void setLeftChild(TreeNode node) {
		this.leftChild = node;
	}

	public TreeNode getRightChild() {
		return this.leftChild;
	}

	public void setRightChild(TreeNode node) {
		this.leftChild = node;
	}
}
TreeNode root = new TreeNode(50);
TreeNode node40 = new TreeNode(40);
TreeNode node70 = new TreeNode(70);
TreeNode node30 = new TreeNode(30);
TreeNode node45 = new TreeNode(45);
TreeNode node60 = new TreeNode(60);
root.setLeftChild(node40);
root.setRightChild(node70);
node40.setLeftChild(node30);
node40.setRightChild(node45);
node70.setLeftChild(node60);
TreeNode binary_search(TreeNode node, double value) {
	if (node == null) {
		return null;
	}
	if (node.getValue() == value) {
		return node;
	} else if (node.getValue() < value) {
		return binary_search(node.getRightChild(), value);
	} else {
		return binary_search(node.getLeftChild(), value);
	}
}

System.out.println(binary_search(root, 60));
