//DEPS com.github.freva:ascii-table:1.8.0
import com.github.freva.asciitable.*;
import java.io.*;
import java.util.*;
/*#jCake:remove*/class test1681857603014{
/*/#jCake:remove*//*#jCake:remove*/	public static void main(String [] args) throws Exception{
/*/#jCake:remove*//*#jCake:movedBlockSource(0)*/	class test{
		public int add(int a, int b){
			return a+b;
		}
	}/*/#jCake:movedBlockSource(0)*/

	boolean [][] cases=new boolean[][] {
	    {false, false},{false, true}, {true, false}, {true, true}
	};
	String[] headers = {"p", "q", "!(p && q)", "!p || !q"};
	List<String[]> rows=new ArrayList<>();
	for(int i=0; i<cases.length; i++) {
		rows.add(new String[] {""+cases[i][0], ""+cases[i][1], ""+(!(cases[i][0] && cases[i][1])), ""+(!cases[i][0] || !cases[i][1])   });
	}
/*#jCake:movedBlock(0)*/
	String [][] data=rows.toArray(new String[0][0]);
	System.out.println(AsciiTable.getTable(headers, data));
	for(int i=0; i<100; i++)
	{
	    for(int j=0; j<100; j++){
	        System.out.println(i*j);
	    }
	}
	/*#jCake:movedBlock(1)*/
	/*#jCake:movedBlock(2)*/
	test();
	test t=new test();
	System.out.println(t.add(1,2));
	/*#jCake:remove*/}/*/#jCake:remove*/
/*#jCake:movedBlockSource(1)*/	/*#jCake:remove*/static /*/#jCake:remove*/void test(){
	
	}
/*/#jCake:movedBlockSource(1)*//*#jCake:movedBlockSource(2)*/	/*#jCake:remove*/static /*/#jCake:remove*/int test2(int a, int b){
	    return 1;
	}
/*/#jCake:movedBlockSource(2)*/
/*#jCake:remove*/}/*/#jCake:remove*/
