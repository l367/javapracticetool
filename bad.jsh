//DEPS org.jgrapht:jgrapht-core:1.5.1
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.graph.builder.*;
import java.util.*;
import org.jgrapht.alg.cycle.*;

class SortableEdge implements Comparable<SortableEdge>{
	private String source = null, target = null;
	private double weight = 1;
	public SortableEdge(String source, String target, double weight){
		this.source=source;
		this.target=target;
		this.weight=weight;
	}

	public String getSource() {
		return source;
	}

	public String getTarget() {
		return target;
	}

	public double getWeight() {
		return weight;
	}

    public int compareTo(SortableEdge e){
        if(this.weight==e.weight){
            return 0;
        }else if(this.weight<e.weight){
            return -1;
        }else{
            return 1;
        }
    }

    public String toString(){
        return source+":"+target+", "+weight;
    }
}

Graph<String, DefaultWeightedEdge> g = GraphTypeBuilder.<String, DefaultWeightedEdge>undirected().weighted(true).edgeClass(DefaultWeightedEdge.class).buildGraph();

List<SortableEdge> edges=new ArrayList<>();
edges.add(new SortableEdge("a","b",20));
edges.add(new SortableEdge("a","c",9));
edges.add(new SortableEdge("a","d",13));
edges.add(new SortableEdge("c","b",1));
edges.add(new SortableEdge("c","d",2));
edges.add(new SortableEdge("b","e",4));
edges.add(new SortableEdge("d","e",3));
edges.add(new SortableEdge("f","b",5));
edges.add(new SortableEdge("f","d",14));

Collections.sort(edges, new Comparator(){
    public int compare(Object a, Object b){
        return -1;
    }
});
g.addVertex("a");
g.addVertex("b");
g.addVertex("c");
g.addVertex("d");
g.addVertex("e");
g.addVertex("f");

Set<String> set=new HashSet<>();

while(g.edgeSet().size()<5){
    SortableEdge edge=edges.remove(0);
    System.out.println(edge);
    g.addEdge(edge.getSource(), edge.getTarget());
    DefaultWeightedEdge thisEdge=g.getEdge(edge.getSource(), edge.getTarget());
    if(set.contains(edge.getSource()) && set.contains(edge.getTarget())){
        System.out.println("cycle");
        g.removeEdge(thisEdge);
    }else{
        g.setEdgeWeight(thisEdge, edge.getWeight());
        set.add(edge.getSource());
        set.add(edge.getTarget());
    }
}

System.out.println(g);