//DEPS org.jgrapht:jgrapht-core:1.5.1
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.graph.builder.*;
import java.util.*;
import org.jgrapht.alg.tour.*;

Graph<Integer, DefaultEdge> g=
GraphTypeBuilder.<Integer, DefaultEdge> directed().edgeClass(DefaultEdge.class).buildGraph();
g.addVertex(50);
g.addVertex(40);
g.addVertex(70);
g.addVertex(30);
g.addVertex(45);
g.addVertex(60);

g.addEdge(50, 40);
g.addEdge(50, 70);
g.addEdge(40, 30);
g.addEdge(40, 45);
g.addEdge(70, 60);

System.out.println(g);
boolean bs(Graph<Integer, DefaultEdge> g, Integer v, int value){
    if(v.intValue()==value){
        return true;
    }else{
        List<DefaultEdge> edges=new ArrayList<>(g.outgoingEdgesOf(v));
        if(edges==null || edges.isEmpty()){
            return false;
        }
        if(v.intValue()>value){
            return bs(g, g.getEdgeTarget(edges.get(0)),value);
        }else{
            if(edges.size()<2){
                return false;
            }
            return bs(g, g.getEdgeTarget(edges.get(1)),value);
        }
    }
}
System.out.println(bs(g, 50, 60));
