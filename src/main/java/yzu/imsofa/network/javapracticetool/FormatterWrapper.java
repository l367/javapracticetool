/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.jboss.forge.roaster.Roaster;

/**
 *
 * @author lendle
 */
public class FormatterWrapper {
    public static String format(String src) throws Exception{
        return Roaster.format(src);

//        return new Formatter().formatSource(src);
//        ASFormatter formatter=new ASFormatter();
//        formatter.setJavaStyle();
//        formatter.setTabIndentation(4);
//        formatter.setOperatorPaddingMode(false);
//        StringWriter stringWriter=new StringWriter();
//        
//        try(Reader in = new BufferedReader(new StringReader(src)); PrintWriter writer=new PrintWriter(stringWriter)){
//            ASStreamIterator streamIterator = new ASStreamIterator(in);
//            formatter.init(streamIterator);
//		// format the file
//		while (formatter.hasMoreLines())
//		{
//			writer.println(formatter.nextLine().toString());
//
//		}
//        }
//        
//        System.out.println(Roaster.format(src));
//        
//        return stringWriter.toString();
    }
    
    public static void main(String [] args) throws Exception{
        String src = FileUtils.readFileToString(new File("bad.jsh"), "utf-8");
        System.out.println(FormatterWrapper.format(src));
    }
}
