/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import java.io.BufferedReader;
import java.io.Reader;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author USER
 */
public class OutputRedirectThread extends Thread {

    private TextArea textArea = null;
    private BufferedReader logReader = null;

    public OutputRedirectThread() {
        this.setDaemon(true);
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }

    public BufferedReader getLogReader() {
        return logReader;
    }

    public void setLogReader(BufferedReader logReader) {
        this.logReader = logReader;
        this.interrupt();
    }

    public void run() {
        while (true) {
            try {
                if (logReader == null) {
                    Thread.sleep(1000);
                } else {
                    String output = logReader.readLine();
                    if (output != null && output.trim().length() > 0) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                textArea.appendText(output+"\r\n");
                                textArea.setScrollTop(Double.MAX_VALUE);
                            }
                        });
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
