/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import eu.mihosoft.monacofx.MonacoFX;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author USER
 */
public class FileTab extends Tab {

    private File file = null;
    private MonacoFX mfx = null;
    private String prevSavedContent = null;

    public FileTab() {
        super("Untitled (*)");
        init();
    }

    public FileTab(File file) throws IOException {
        super(file.getName());
        this.file = file;
        init();
        String content = FileUtils.readFileToString(file, "utf-8");
        mfx.getEditor().getDocument().setText(content);
        prevSavedContent = content;
    }

    private void init() {
        mfx = new MonacoFX();
        this.setContent(mfx);
        mfx.getEditor().setCurrentLanguage("java");
        mfx.getEditor().setCurrentTheme("vs-dark");
        mfx.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                updateUIForModificationState();
                /*if (((!event.isAltDown() && !event.isControlDown() && !event.isShiftDown())
                        && (event.getCode().isKeypadKey() || event.getCode().isLetterKey() || event.getCode().isWhitespaceKey() || event.getCode().isDigitKey() || event.getCode().equals(KeyCode.BACK_SPACE)))
                        || (event.isControlDown() && event.getCode().equals(KeyCode.Z))) {
                    updateUIForModificationState();
                }*/
            }
        });
        this.setOnCloseRequest((e) -> {
            if (isModified()) {
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Modified");
                alert.setHeaderText("" + file);
                alert.setContentText("File Modified, save?");
                Optional<ButtonType> ret=alert.showAndWait();
                if(ret.isPresent() && ret.get().equals(ButtonType.OK)){
                    FileChooser fc=new FileChooser();
                    if(file!=null){
                        fc.setInitialDirectory(file.getParentFile());
                    }
                    try {
                        save(getTabPane().getScene().getWindow(), fc, false);
                    } catch (IOException ex) {
                        Logger.getLogger(FileTab.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public File getFile() {
        return file;
    }

    public MonacoFX getMfx() {
        return mfx;
    }

    public void save(Window stage, FileChooser fileChooser, boolean saveAs) throws IOException {
        if (this.getFile() == null || saveAs) {
            try {
                File file = fileChooser.showSaveDialog(stage);
                if(file==null){
                    return;
                }
                if (file.getName().endsWith(".java") == false && file.getName().endsWith(".jsh") == false) {
                    final Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Unsupported File Name");
                    alert.setHeaderText("" + file.getName());
                    alert.setContentText("Only .java and .jsh are supported.");
                    alert.showAndWait();
                    return;
                }
                if (file != null) {
                    FileUtils.write(file, getEditorContent(), "utf-8");
                    this.file = file;
                    this.setText(file.getName());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            FileUtils.write(this.getFile(), getEditorContent(), "utf-8");
        }
        prevSavedContent = getEditorContent();
        updateUIForModificationState();
    }

    private String getEditorContent() {
        return ((MonacoFX) this.getContent()).getEditor().getDocument().getText();
    }

    public void reformat() {
        try {
            if (getFile() == null) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning");
                alert.setContentText("Only saved file can be formatted.");
                alert.showAndWait();
                return;
            }
            String current = mfx.getEditor().getDocument().getText();
            String formatted = null;
            if (getFile().getName().toLowerCase().endsWith(".jsh")) {
                formatted = FormatterWrapper.format(ConversionUtil.jshToJava(current, true));
                //have to recover back to jsh
                formatted = ConversionUtil.annotatedJavaToJsh(formatted);
            } else {
                formatted = FormatterWrapper.format(current);
            }

            mfx.getEditor().getDocument().updateText(formatted);
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        updateUIForModificationState();
    }

    public String getSelectedText() {
        String text = "" + mfx.getWebEngine().executeScript("window.getSelection()");
        return text;
    }

    public boolean isModified() {
        return prevSavedContent == null || !prevSavedContent.equals(getEditorContent());
    }

    private void updateUIForModificationState() {
        if (isModified()) {
            if (getText().endsWith("(*)") == false) {
                setText(getText() + "(*)");
            }
        } else {
            setText(getText().replace("(*)", ""));
        }
    }

}
