/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.jastyle;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;

/**
 *
 * @author lendle
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
//        Main.main(new String[]{"--mode=java", "--style=java", "--indent=tab","test.java"});
        ASFormatter formatter=new ASFormatter();
        formatter.setJavaStyle();
        formatter.setTabIndentation(4);
        formatter.setOperatorPaddingMode(false);
        try(Reader in = new BufferedReader(new FileReader("bad.java"))){
            ASStreamIterator streamIterator = new ASStreamIterator(in);
            formatter.init(streamIterator);

		// format the file
		while (formatter.hasMoreLines())
		{
			System.out.println(formatter.nextLine().toString());
			// if (formatter.hasMoreLines())
			// out.print(streamIterator.getOutputEOL());

		}
        }
        
    }
    
}
