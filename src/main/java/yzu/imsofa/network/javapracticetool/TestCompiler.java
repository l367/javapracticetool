/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.Problem;
import com.github.javaparser.ast.CompilationUnit;
import org.codehaus.commons.compiler.CompileException;
import org.codehaus.commons.compiler.ErrorHandler;
import org.codehaus.commons.compiler.Location;
import org.codehaus.janino.SimpleCompiler;

/**
 *
 * @author USER
 */
public class TestCompiler {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //String code = "public cass HelloWorld { public static void main(String[] args) { Sytem.out.printl(\"Hello, world!\"); } }";
        String code = "Sytem.out.printl(\"Hello, world!\"); ";
        try {
            /*ParseResult<CompilationUnit> cu = new JavaParser().parse(code);
            System.out.println(cu.getResult().isPresent());
            if (cu.getProblems().isEmpty() == false) {
                for (Problem p : cu.getProblems()) {
                    System.out.println(p);
//                    System.out.println(p.getLocation().get().toRange().get().begin.line + ":" + p.getMessage());
                }
            } */
            {
                SimpleCompiler ee = new SimpleCompiler();
                ee.setCompileErrorHandler(new ErrorHandler() {
                    @Override
                    public void handleError(String string, Location lctn) throws CompileException {
                        System.out.println("123:" + lctn + ":" + string);
                        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
                    }
                });
                ee.cook(code);
            }
        } catch (CompileException e) {
            System.out.println(e.getLocation().getLineNumber());
            e.printStackTrace();
        }
    }

}
