/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author USER
 */
public class ConversionUtil {

    public static String jshToJava(String jshSource) throws IOException {
        return jshToJava(jshSource, false);
    }
    
    protected static String extractMethodName(String methodHead){
        int index=methodHead.indexOf("(")-1;
        char c=methodHead.charAt(index);
        while(c==' '){
            index--;
            c=methodHead.charAt(index);
        }
        int index0=index--;
        c=methodHead.charAt(index);
        while(c!=' '){
            index0--;
            c=methodHead.charAt(index0);
        }
        return methodHead.substring(index0+1, index+2);
    }
    
    public static String jshToJava(String jshSource, boolean addRecoverAnnotation) throws IOException {
        int idMovedBlock = 0;
        List<String> lines = IOUtils.readLines(new StringReader(jshSource));
        StringBuffer result = new StringBuffer();
        StringBuffer toBeProcessed = new StringBuffer();
        StringBuffer classes = new StringBuffer();
        StringBuffer methods = new StringBuffer();
        boolean codeStart = false;
        for (String line : lines) {
            if (!codeStart) {
                if (line.startsWith("import") || line.startsWith("package") || line.startsWith("//") || line.isBlank()) {
                    result.append(line).append("\r\n");
                } else {
                    codeStart = true;
                    if (addRecoverAnnotation) {
                        result.append("/*#jCake:remove*/\r\nclass test" + System.currentTimeMillis() + "{\r\n/*/#jCake:remove*/");
                        result.append("/*#jCake:remove*/\r\n\tpublic static void main(String [] args) throws Exception{\r\n/*/#jCake:remove*/");
                    } else {
                        result.append("class test" + System.currentTimeMillis() + "{\r\n");
                        result.append("\tpublic static void main(String [] args) throws Exception{\r\n");
                    }
                    toBeProcessed.append("\t").append(line).append("\r\n");
                }
            } else {
                toBeProcessed.append("\t").append(line).append("\r\n");
            }
        }

        String src = toBeProcessed.toString();
        Pattern classHeaderPattern = Pattern.compile("[^;})\"\r\n]*(class)(\\s)*[^\\{;(]+\\{");
        while (true) {
            Matcher m = classHeaderPattern.matcher(src);
            if (m.find()) {
                StringBuffer classBlockContent = new StringBuffer(m.group());
                //consume the whole method block using { matching
                int blockCharIndex = m.end();
                Stack stack = new Stack();
                stack.push("{");
                while (!stack.isEmpty()) {
                    char c = src.charAt(blockCharIndex);
                    classBlockContent.append(c);
                    if (c == '}') {
                        stack.pop();
                    } else if (c == '{') {
                        stack.push("{");
                    }
                    blockCharIndex++;
                }
                if (addRecoverAnnotation) {
                    src = src.replace(classBlockContent.toString(), "/*#jCake:movedBlock(" + idMovedBlock + ")*/");
                    String cls = "\t/*#jCake:remove*/\r\nstatic /*/#jCake:remove*/" + classBlockContent.toString().trim();
                    classes.append("/*#jCake:movedBlockSource(" + idMovedBlock + ")*/" + cls).append("/*/#jCake:movedBlockSource(" + idMovedBlock + ")*/\r\n");
                    idMovedBlock++;
                } else {
                    src = src.replace(classBlockContent.toString(), "");
                    String cls = "\tstatic " + classBlockContent.toString().trim();
                    classes.append(cls).append("\r\n");
                }
            } else {
                break;
            }
        }

//        Pattern methodHeaderPattern = Pattern.compile("(public|private|static|protected|abstract|native|synchronized)*([a-zA-Z0-9<>._?, ]+) +([a-zA-Z0-9_]+) *\\([a-zA-Z0-9<>\\[\\]._?, \\n]*\\) *([a-zA-Z0-9_ ,\\n]*) *\\{");
//        Pattern methodHeaderPattern = Pattern.compile("\\b(public|private|static|protected|abstract|native|synchronized)*(([a-zA-Z0-9<>_]+)+(\\s+))+([a-zA-Z0-9_]+)+(\\s)*\\([a-zA-Z0-9<>\\[\\]._?, \\n]*\\) *([a-zA-Z0-9_ ,\\n]*) *\\{");
//        Pattern methodHeaderPattern = Pattern.compile("\\b(public|private|static|protected|abstract|native|synchronized)*\\w+\\s+(\\w+\\s+)*\\w+\\s*\\([^{}]*\\)\\s*\\{");
        Pattern methodHeaderPattern = Pattern.compile("\\b(public|private|static|protected|abstract|native|synchronized)*(\\w+\\s+)+\\w+\\s*\\([\\w<>\\[\\]._?,\\s\\n]*\\)\\s*[\\w,\\s\\n]*\\{");
        int startMatchIndex=0;
        while (true) {
            Matcher m = methodHeaderPattern.matcher(src);    
            if (m.find(startMatchIndex)) {
                StringBuffer methodBlockContent = new StringBuffer(m.group());
                //consume the whole method block using { matching
                int blockCharIndex = m.end();
                Stack stack = new Stack();
                stack.push("{");
                while (!stack.isEmpty()) {
                    char c = src.charAt(blockCharIndex);
                    methodBlockContent.append(c);
                    if (c == '}') {
                        stack.pop();
                    } else if (c == '{') {
                        stack.push("{");
                    }
                    blockCharIndex++;
                }
//                System.out.println("group: "+m.group());
                int checkBeginIndex=m.start();
                char c1=src.charAt(blockCharIndex);
                boolean canAccept=true;
                while(c1!=';'){
                    if(c1=='('){
                        canAccept=false;
                        break;
                    }
                    checkBeginIndex--;
                    c1=src.charAt(checkBeginIndex);
                }
                if(!canAccept){
                    startMatchIndex=m.end()+1;
                    continue;
                }
                if (addRecoverAnnotation) {
                    src = src.replace(methodBlockContent.toString(), "/*#jCake:movedBlock(" + idMovedBlock + ")*/");
                    String method = "\t/*#jCake:remove*/\r\nstatic /*/#jCake:remove*/" + methodBlockContent.toString().trim();
                    methods.append("/*#jCake:movedBlockSource(" + idMovedBlock + ")*/" + method).append("\r\n/*/#jCake:movedBlockSource(" + idMovedBlock + ")*/");
                    idMovedBlock++;
                } else {
                    src = src.replace(methodBlockContent.toString(), "");
                    String method = "\tstatic " + methodBlockContent.toString().trim();
                    methods.append(method).append("\r\n");
                }
            } else {
                break;
            }
        }
        
        result.append(src.replaceAll("([ \\t\\n\\x0B\\f\\r](\\n)[ \\t\\n\\x0B\\f\\r])+", "\r\n\t"));
        if (addRecoverAnnotation) {
            result.append("\t/*#jCake:remove*/\r\n}/*/#jCake:remove*/\r\n");
        } else {
            result.append("\t}\r\n");
        }
        result.append(classes).append("\r\n");
        result.append(methods).append("\r\n");
        if (addRecoverAnnotation) {
            result.append("/*#jCake:remove*/\r\n}/*/#jCake:remove*/");
        } else {
            result.append("}");
        }
        return result.toString();
    }

    /**
     * convert annotated java source (with ConversionUtil.jshToJava with
     * recoverAnnotation)
     *
     * @param src
     * @return
     */
    public static String annotatedJavaToJsh(String src) {
        String result = src;
//        System.out.println(src);
//        Pattern annotationPattern = Pattern.compile("/(\\*(\\s)+)+(/)?#jCake:[^\\*]+(\\*)/");
        Pattern annotationPattern = Pattern.compile("\\#([\\s\\*])*j([\\s\\*])*C([\\s\\*])*a([\\s\\*])*k([\\s\\*])*e([\\s\\*])*:[^/]*/");
        Matcher m = null;
        //replace additional space in annotations (the underlying formatter, Roater, may generate un-required spaces in comments)
        outer:
        while (true) {
            m = annotationPattern.matcher(result);
            boolean matched = false;
            if (m.find()) {
                matched = true;
                String current = m.group();
                //find the first / before this
                int index = m.start() - 1;
                char c = result.charAt(index);
                int slashCount = 0;
                boolean starEncountered=false;
                while (index > 0 && (c == '/' || c == '*' || Character.isWhitespace(c) || c == '\r' || c == '\t' || c == ' ')) {
                    if(c=='*'){
                        starEncountered=true;
                    }else if(c=='/'){
                        slashCount++;
                        if(starEncountered){
                            break;
                        }
                    }
                    index--;
                    c = result.charAt(index);
                }
                index--;
                current = current.replaceAll("(\\s)+", "").replace("*", "");
                current = current.substring(0, current.length() - 1) + "*/";
                if (slashCount <= 1) {
                    result = result.substring(0, index + 1) + "/*@" + current.substring(1) + result.substring(m.end());
                } else {
                    result = result.substring(0, index + 1) + "/*/@" + current.substring(1) + result.substring(m.end());
                }
                continue outer;
            }
            if (!matched) {
                break;
            }
        }
        result = result.replace("@jCake:", "#jCake:");
        //////////////////////////////////////////
        //locate all movedBlockSource annotations, prepare the moveBackMap
        annotationPattern = Pattern.compile("/\\*(/)?#jCake:[^\\*]+(\\*)/");
        m = annotationPattern.matcher(result);
        Stack<AnnotationDesc> stack = new Stack<>();
        Map<String, String> moveBackMap = new HashMap<>();
        List<String> removeList = new ArrayList<>();
        while (m.find()) {
//            System.out.println(m.start()+":"+m.group()+":"+m.end());
            if (m.group().startsWith("/*#jCake:movedBlockSource(")) {
                AnnotationDesc ad = new AnnotationDesc();
                ad.setGroup(m.group());
                ad.setStart(m.start());
                ad.setEnd(m.end());
                stack.push(ad);
            } else if (m.group().startsWith("/*/#jCake:movedBlockSource(")) {
                try {
                    AnnotationDesc annotationDesc = stack.pop();
                    int index1 = annotationDesc.group.indexOf("(") + 1;
                    int index2 = annotationDesc.group.lastIndexOf(")");
                    String numberIndex = annotationDesc.group.substring(index1, index2);
                    moveBackMap.put(numberIndex, result.substring(annotationDesc.end, m.start()));
                    index1 = annotationDesc.start;
                    index2 = m.end();
                    removeList.add(result.substring(index1, index2));
                } catch (Throwable e) {
//                    System.out.println("stuck at " + m.group());
                    throw new RuntimeException(e);
                }
//                System.out.println("removing: " + result.substring(index1, index2));
            }
        }
        //move back all movedBlockSource
        for (String numberIndex : moveBackMap.keySet()) {
            result = result.replace("/*#jCake:movedBlock(" + numberIndex + ")*/", moveBackMap.get(numberIndex));
        }
        stack.clear();
        //remove all nested annotation blocks
        outer:
        while (true) {
            m = annotationPattern.matcher(result);
            boolean matched = false;
            while (m.find()) {
                matched = true;
                if (m.group().startsWith("/*#jCake:")) {
                    AnnotationDesc ad = new AnnotationDesc(m);
                    stack.push(ad);
                } else if (m.group().startsWith("/*/#jCake:")) {
//                    System.out.println("group=" + m.group() + ":" + result.substring(m.start() - 50, m.end()));
                    AnnotationDesc annotationDesc = stack.pop();
                    AnnotationDesc annotationDesc2 = new AnnotationDesc(m);
                    String inner = result.substring(annotationDesc.start, annotationDesc2.end);
//                    System.out.println("inner="+inner);
                    result = result.replace(inner, "");
                    continue outer;
                }

            }
            if (!matched) {
                break;
            }
        }

        //re-indent
        int indentLevel=0;
        StringBuilder ret = new StringBuilder();
        String[] lines = result.split("\n");
        if (lines != null) {
            boolean prevLineEmpty=false;
            for (String line : lines) {
                line=line.trim();
                if (line.isEmpty()) {
                    if(!prevLineEmpty){
                        prevLineEmpty=true;
                        ret.append("\r\n");
                    }
                    continue;
                }else{
                    prevLineEmpty=false;
                }
                if(ret.length()!=0){
                    ret.append("\r\n");
                }
                if(line.startsWith("}")){
                    indentLevel--;
                }
                for(int i=0; i<indentLevel; i++){
                    ret.append("\t");
                }
                ret.append(line);
                if(line.endsWith("{")){
                    indentLevel++;
                }
            }
        }
        result = ret.toString();
        return result;
    }

    public static void main(String[] args) throws Exception {
        String src=FileUtils.readFileToString(new File("test.jsh"), "utf-8");
        src=ConversionUtil.jshToJava(src, false);
        System.out.println(src);
//        System.out.println(ConversionUtil.annotatedJavaToJsh(src));
        /*Pattern methodHeaderPattern = Pattern.compile("\\b(public|private|static|protected|abstract|native|synchronized)*\\w+\\s+(\\w+\\s+)*\\w+\\s*\\([^{}]*\\)\\s*\\{");
        String src="   String test(){\r\nSystem.out.println(\"123\");\r\n}";
        Matcher m=methodHeaderPattern.matcher(src);
        System.out.println(m.find()+":"+m.group());
        
        src="  if(b){";
        m=methodHeaderPattern.matcher(src);
        System.out.println(m.find()+":"+m.group());
        src=FileUtils.readFileToString(new File("dijk.jsh"), "utf-8");
        m=methodHeaderPattern.matcher(src);
        while(m.find()){
            System.out.println(m.group());
        }*/
        /*String src = FileUtils.readFileToString(new File("bad.jsh"), "utf-8");
//        String src="123;a b(){}";
//        String result = src;
        String result = ConversionUtil.jshToJava(src, true);
//        System.out.println(result);
        result = FormatterWrapper.format(result);
//        System.out.println("roaster: "+result);
        System.out.println(annotatedJavaToJsh(result));*/
    }
    
    public static File createTempJavaFileFromJsh(File jshFile, String code) throws IOException {
        File tempDir = new File(jshFile.getParent(), ".tmp");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        File tempFile = new File(tempDir, "test" + System.currentTimeMillis() + ".java");
        FileUtils.write(tempFile, ConversionUtil.jshToJava(code), "utf-8");
        return tempFile;
    }

    static class AnnotationDesc {

        private String group = null;
        private String simpleName = null;
        private int start = -1;
        private int end = -1;

        public AnnotationDesc() {
        }

        public AnnotationDesc(Matcher m) {
            this.setGroup(m.group());
            this.start = m.start();
            this.end = m.end();
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
            int index1 = group.indexOf(":") + 1;
            int index2 = group.indexOf("(");
            if (index2 == -1) {
                index2 = group.lastIndexOf("*");
            }
            this.simpleName = group.substring(index1, index2);
        }

        public String getSimpleName() {
            return simpleName;
        }

        public int getStart() {
            return start;
        }

        public void setStart(int start) {
            this.start = start;
        }

        public int getEnd() {
            return end;
        }

        public void setEnd(int end) {
            this.end = end;
        }

    }
}
