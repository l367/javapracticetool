/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.actions;

import java.util.List;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.web.WebView;

/**
 *
 * @author lendle
 */
public class SearchWebAction extends AbstractUIAction {

    private TabPane tabPaneSearchResults = null;
    private String keyword = null;
    private WebView webView = null;

    public SearchWebAction(TabPane tabPaneSearchResults, String keyword) {
        super(List.of());
        this.tabPaneSearchResults = tabPaneSearchResults;
        this.keyword = keyword;
    }

    @Override
    protected void runInWorkerThread() {

    }

    @Override
    protected void runInUIThread(boolean beforeWorkerThread) {
        if (beforeWorkerThread == false) {
            webView = new WebView();
            webView.getEngine().setJavaScriptEnabled(true);
            String userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36";
            webView.getEngine().setUserAgent(userAgent);
            webView.getEngine().load("https://cse.google.com/cse?cx=71f5da24e09e84c9a#gsc.tab=0&gsc.q=" + keyword + "&gsc.sort=");
            Tab tab = new Tab(keyword, webView);
            tab.setClosable(true);
            tabPaneSearchResults.getTabs().add(tab);
        }
    }

}
