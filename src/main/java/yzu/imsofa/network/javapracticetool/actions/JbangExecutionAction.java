/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.actions;

import com.kodedu.terminalfx.TerminalTab;
import java.io.File;
import java.util.List;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import org.apache.commons.io.FileUtils;
import yzu.imsofa.network.javapracticetool.ConversionUtil;
import yzu.imsofa.network.javapracticetool.FileTab;
import yzu.imsofa.network.javapracticetool.ThreadUtil;
import yzu.imsofa.network.javapracticetool.codechecker.Checker;
import yzu.imsofa.network.javapracticetool.codechecker.CodeIssue;

/**
 *
 * @author USER
 */
public class JbangExecutionAction extends AbstractUIAction {

    private TabPane tabPane = null;
    private boolean runAsJava = false;
    private File jbangExecutable = null;
    private TerminalTab terminal = null;
    private ObservableList<CodeIssue> codeIssuesList = null;
    private Tab tabCheck = null;
    private File javaFile = null;
    private String javaCode = null;

    public JbangExecutionAction(List<Node> managedControls, File jbangExecutable, TabPane tabPane, boolean runAsJava,
            TerminalTab terminal, Tab tabCheck, ObservableList<CodeIssue> codeIssuesList) {
        super(managedControls);
        this.jbangExecutable = jbangExecutable;
        this.tabPane = tabPane;
        this.runAsJava = runAsJava;
        this.terminal = terminal;
        this.tabCheck = tabCheck;
        this.codeIssuesList = codeIssuesList;
    }

    @Override
    protected void runInWorkerThread() {
        try {
            FileTab tab = (FileTab) tabPane.getSelectionModel().getSelectedItem();
            if (tab == null) {
                return;
            }
            String code = tab.getMfx().getEditor().getDocument().getText();
            String os = System.getProperty("os.name").toLowerCase();
            javaFile = tab.getFile();
            javaCode = code;
            if (tab.getFile().getName().toLowerCase().endsWith("java") == false) {
                //need javaCode for checking
                javaFile = ConversionUtil.createTempJavaFileFromJsh(tab.getFile(), code);
                javaCode = FileUtils.readFileToString(javaFile, "utf-8");
            }
            ThreadUtil.execute(() -> {
                List<CodeIssue> issues = Checker.getDefaultChecker().check(javaFile, javaCode);
                Platform.runLater(() -> {
                    codeIssuesList.clear();
                    codeIssuesList.addAll(issues);
                    if (issues != null && !issues.isEmpty()) {
                        //tabCheck.getTabPane().getSelectionModel().select(tabCheck);
                    }
                });
            });

            if (runAsJava) {
                //run with java compiler
                terminal.getTerminal().command("cd \"" + javaFile.getParentFile().getCanonicalPath() + "\"\r");
                if (os.startsWith("win")) {
                    int index = javaFile.getParentFile().getCanonicalPath().indexOf(":");
                    if (index < 5) {
                        terminal.getTerminal().command(javaFile.getParentFile().getCanonicalPath().substring(0, index + 1) + "\r\n");
                    }
                }
                terminal.getTerminal().command("\"" + jbangExecutable.getCanonicalPath() + "\" \"" + javaFile.getCanonicalPath() + "\"\r");
            } else {
                //run with jshell
                terminal.getTerminal().command("cd \"" + tab.getFile().getParentFile().getCanonicalPath() + "\"\r");
                if (os.startsWith("win")) {
                    int index = tab.getFile().getParentFile().getCanonicalPath().indexOf(":");
                    if (index < 5) {
                        terminal.getTerminal().command(tab.getFile().getParentFile().getCanonicalPath().substring(0, index + 1) + "\r\n");
                    }
                }
                terminal.getTerminal().command("\"" + jbangExecutable.getCanonicalPath() + "\" \"" + tab.getFile().getCanonicalPath() + "\"\r");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void runInUIThread(boolean beforeWorkerThread) {

    }
}
