/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.actions;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.Node;
import yzu.imsofa.network.javapracticetool.ThreadUtil;

/**
 *
 * @author lendle
 */
public abstract class AbstractUIAction {

    private List<Node> managedControls = new ArrayList<>();

    public AbstractUIAction(List<Node> managedControls) {
        this.managedControls.addAll(managedControls);
    }

    protected abstract void runInWorkerThread();

    protected abstract void runInUIThread(boolean beforeWorkerThread);

    public void execute() {
        ThreadUtil.execute(() -> {
            Platform.runLater(() -> {
                List<Boolean> originStatus = new ArrayList<>();
                for (Node node : managedControls) {
                    originStatus.add(node.isDisable());
                    node.setDisable(true);
                }
                runInUIThread(true);
                ThreadUtil.execute(() -> {
                    runInWorkerThread();
                    Platform.runLater(() -> {
                        runInUIThread(false);
                        for (int i = 0; i < originStatus.size(); i++) {
                            Node node = managedControls.get(i);
                            node.setDisable(originStatus.get(i));
                        }
                    });
                });
            });

        });
    }
}
