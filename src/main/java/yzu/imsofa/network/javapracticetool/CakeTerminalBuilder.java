/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import com.kodedu.terminalfx.TerminalBuilder;
import com.kodedu.terminalfx.TerminalTab;
import com.kodedu.terminalfx.config.TerminalConfig;

/**
 *
 * @author USER
 */
public class CakeTerminalBuilder extends TerminalBuilder{

    public CakeTerminalBuilder() {
    }

    public CakeTerminalBuilder(TerminalConfig terminalConfig) {
        super(terminalConfig);
    }
    
    public TerminalTab newTerminal() {
        TerminalTab terminalTab = new CakeTerminalTab(getTerminalConfig(), getNameGenerator(), getTerminalPath());
        return terminalTab;
    }
}
