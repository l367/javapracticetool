/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import com.kodedu.terminalfx.Terminal;
import com.kodedu.terminalfx.TerminalTab;
import com.kodedu.terminalfx.config.TabNameGenerator;
import com.kodedu.terminalfx.config.TerminalConfig;
import java.nio.file.Path;

/**
 *
 * @author USER
 */
public class CakeTerminalTab extends TerminalTab {
    public CakeTerminalTab(TerminalConfig terminalConfig, TabNameGenerator tabNameGenerator, Path terminalPath) {
        super(new CakeTerminal(terminalConfig, terminalPath), tabNameGenerator);
    }
}
