/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.codechecker;

import java.io.File;
import java.util.List;

/**
 *
 * @author lendle
 */
public interface Checker {
    public List<CodeIssue> check(File fileName, String src);
    public static Checker getDefaultChecker(){
        return new JavaParserCheckerImpl();
    }
}
