/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.codechecker;

/**
 *
 * @author lendle
 */
public class CodeIssue {
    private int line=-1;
    private String message=null;
    private IssueType issueType=null;

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IssueType getIssueType() {
        return issueType;
    }

    public void setIssueType(IssueType issueType) {
        this.issueType = issueType;
    }

    @Override
    public String toString() {
        return "CodeIssue{" + "line=" + line + ", message=" + message + ", issueType=" + issueType + '}';
    }
    
    
}
