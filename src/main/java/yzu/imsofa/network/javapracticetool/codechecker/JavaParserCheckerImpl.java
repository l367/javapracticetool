/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.codechecker;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.Problem;
import com.github.javaparser.ast.CompilationUnit;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author lendle
 */
public class JavaParserCheckerImpl implements Checker {

    @Override
    public List<CodeIssue> check(File fileName, String src) {
        List<CodeIssue> issues = new ArrayList<>();
        ParseResult<CompilationUnit> cu = new JavaParser().parse(src);
        if (cu.getProblems().isEmpty() == false) {
                for (Problem p : cu.getProblems()) {
                    CodeIssue issue=new CodeIssue();
                    issue.setIssueType(IssueType.CORRECTNESS);
                    issue.setLine(p.getLocation().get().toRange().get().begin.line);
                    issue.setMessage(p.getMessage());
                    issues.add(issue);
                }
            }
        Collections.sort(issues, new Comparator<CodeIssue>(){
            @Override
            public int compare(CodeIssue o1, CodeIssue o2) {
                return o1.getLine()-o2.getLine();
            }
        });
        return issues;
    }

}
