/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.codechecker;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.commons.compiler.CompileException;
import org.codehaus.commons.compiler.ErrorHandler;
import org.codehaus.commons.compiler.Location;
import org.codehaus.janino.SimpleCompiler;

/**
 *
 * @author lendle
 */
public class JaninoCheckerImpl implements Checker {

    @Override
    public List<CodeIssue> check(File fileName, String src) {
        List<CodeIssue> issues = new ArrayList<>();
        SimpleCompiler ee = new SimpleCompiler();
        ee.setCompileErrorHandler(new ErrorHandler() {
            @Override
            public void handleError(String string, Location lctn) throws CompileException {
                CodeIssue issue = new CodeIssue();
                issue.setIssueType(IssueType.CORRECTNESS);
                issue.setLine(lctn.getLineNumber());
                issue.setMessage(string);
                issues.add(issue);
            }
        });
        try {
            ee.cook(src);
        } catch (CompileException ex) {
            CodeIssue issue = new CodeIssue();
            issue.setIssueType(IssueType.CORRECTNESS);
            issue.setLine((ex.getLocation()!=null)?ex.getLocation().getLineNumber():-1);
            issue.setMessage(ex.getMessage());
            issues.add(issue);
        }
        Collections.sort(issues, new Comparator<CodeIssue>(){
            @Override
            public int compare(CodeIssue o1, CodeIssue o2) {
                return o1.getLine()-o2.getLine();
            }
        });
        return issues;
    }

}
