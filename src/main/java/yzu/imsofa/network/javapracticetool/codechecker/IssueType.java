/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.codechecker;

/**
 *
 * @author lendle
 */
public enum IssueType {
    CORRECTNESS,
    QUALITY
}
