package yzu.imsofa.network.javapracticetool;

import com.github.mouse0w0.darculafx.DarculaFX;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.stage.WindowEvent;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("primary.fxml"));
        Parent parent = fxmlLoader.load();
        scene = new Scene(parent, 1200, 768);
        DarculaFX.applyDarculaStyle(scene);
        final PrimaryController controller = fxmlLoader.getController();
        stage.getIcons().add(new Image(App.class.getResourceAsStream("jcake.png")));
        Platform.setImplicitExit(false);
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (controller.checkBeforeFileClose()) {
                    controller.terminateCurrentProcess();
                    System.exit(0);
                }else{
                    event.consume();
                }
            }
        });
        stage.setScene(scene);
        stage.setTitle("jCake");
        stage.show();
    }

//    static void setRoot(String fxml) throws IOException {
//        scene.setRoot(loadFXML(fxml));
//    }
//    private static Parent loadFXML(String fxml) throws IOException {
//        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
//        return fxmlLoader.load();
//    }
    public static void main(String[] args) {
        launch();
    }

}
