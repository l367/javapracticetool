/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import javafx.application.Platform;

/**
 *
 * @author lendle
 */
public abstract class AsyncExecutor {

    protected abstract void runInWorkerThread();

    protected abstract void runInUiThread(boolean beforeWorkerThread);

    public void run() {
        Exception e = null;
        Thread t = new Thread() {
            public void run() {
                try {
                    Platform.runLater(() -> {
                        runInUiThread(true);
                    });
                    runInWorkerThread();
                } finally {
                    Platform.runLater(() -> {
                        runInUiThread(false);
                    });
                }
            }
        };
        t.start();
    }
}
