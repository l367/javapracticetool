package yzu.imsofa.network.javapracticetool;

import com.kodedu.terminalfx.TerminalBuilder;
import com.kodedu.terminalfx.TerminalTab;
import com.kodedu.terminalfx.config.TerminalConfig;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import rocks.imsofa.rocksfx.table.DefaultTableCellRenderer;
import rocks.imsofa.rocksfx.table.TableColumnEx;
import yzu.imsofa.network.javapracticetool.actions.JbangExecutionAction;
import yzu.imsofa.network.javapracticetool.actions.SearchWebAction;
import yzu.imsofa.network.javapracticetool.codechecker.Checker;
import yzu.imsofa.network.javapracticetool.codechecker.CodeIssue;

public class PrimaryController {

    private FileChooser fileChooser = new FileChooser();
    private File jbangExecutable = null;

    @FXML
    private CheckMenuItem checkboxRunasJava;

    @FXML
    private TabPane tabPane;

    @FXML
    private Button buttonRun;
    @FXML
    private MenuItem menuItemRun;

    @FXML
    private TabPane southTabPane;

    @FXML
    private TabPane tabPaneSearchResults;

    @FXML
    private TextField textSearch;

    private PrintWriter currentWriterToProcess = null;
    private Process currentProcess = null;
    private TerminalTab terminal = null;

    @FXML
    private TableView<CodeIssue> tableCheck;
    private ObservableList<CodeIssue> codeIssuesList=FXCollections.observableArrayList();
    
    @FXML
    private Tab tabCheck;

    public void initialize() {
        try {
            FileUtils.deleteDirectory(new File(".tmp"));
        } catch (IOException ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("all supported files", "*.jsh", "*.java"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("jsh", "*.jsh"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("java", "*.java"));
        String os = System.getProperty("os.name").toLowerCase();
        File jbangDir = new File("jbang");
        File binDir = new File(jbangDir, "bin");
        if (os.contains("win")) {
            jbangExecutable = new File(binDir, "jbang.cmd");
        } else {
            jbangExecutable = new File(binDir, "jbang");
        }
        recreateTerminal();
        TableColumnEx<CodeIssue, Integer> lineColumn=new TableColumnEx<>("Line");
        TableColumnEx<CodeIssue, String> messageColumn=new TableColumnEx<>("Message");
        lineColumn.prefWidthProperty().bind(tableCheck.widthProperty().multiply(0.2));
        messageColumn.prefWidthProperty().bind(tableCheck.widthProperty().multiply(0.8));
        lineColumn.setTableCellRenderer(new DefaultTableCellRenderer<CodeIssue, Integer>("line"));
        messageColumn.setTableCellRenderer(new DefaultTableCellRenderer<>("message"){
            @Override
            public Node getRendererComponent(TableCell<CodeIssue, String> tableCell, CodeIssue object, int index, boolean empty) {
                Label label=(Label) super.getRendererComponent(tableCell, object, index, empty); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
                label.setTooltip(new Tooltip(label.getText()));
                label.setPrefHeight((label.getText().length()/30)*24.0);
                label.setWrapText(true);
                label.setAlignment(Pos.TOP_LEFT);
                return label;
            }
            
        });
        tableCheck.getColumns().clear();
        tableCheck.getColumns().addAll(lineColumn, messageColumn);
        tableCheck.setItems(codeIssuesList);
    }

    private void recreateTerminal() {
        TerminalConfig terminalConfig = new TerminalConfig();
        terminalConfig.setBackgroundColor(Color.web("#3c3f41"));
        terminalConfig.setForegroundColor(Color.WHITE);
        terminalConfig.setScrollWhellMoveMultiplier(2);
        TerminalBuilder terminalBuilder = new CakeTerminalBuilder(terminalConfig);
        terminal = terminalBuilder.newTerminal();
        terminal.setClosable(false);
        southTabPane.getTabs().add(0, terminal);
        redirectKeyReleaseEvent(terminal.getTerminal());
    }

    @FXML
    void onNew(ActionEvent event) {
        FileTab tab = new FileTab();
        redirectKeyReleaseEvent(tab.getMfx());
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
    }

    @FXML
    void OnRun(ActionEvent event) {
        try {
            saveCurrentTab(false);
            new JbangExecutionAction(List.of(buttonRun), jbangExecutable, tabPane, checkboxRunasJava.isSelected(), terminal, tabCheck, codeIssuesList).execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void saveCurrentTab(boolean saveAs) throws IOException {
        FileTab tab = (FileTab) tabPane.getSelectionModel().getSelectedItem();
        tab.save(this.buttonRun.getScene().getWindow(), fileChooser, saveAs);
    }

    @FXML
    void OnOpen(ActionEvent event) {
        try {
            File file = fileChooser.showOpenDialog(tabPane.getScene().getWindow());
            FileTab tab = new FileTab(file);
            redirectKeyReleaseEvent(tab.getMfx());
            tabPane.getTabs().add(tab);
            tabPane.getSelectionModel().select(tab);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    @FXML
    void OnClear(ActionEvent event) {
        terminal.destroy();
        southTabPane.getTabs().remove(terminal);
        recreateTerminal();
    }

    private void redirectKeyReleaseEvent(Node node) {
        node.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.L) && event.isControlDown()) {
                    OnClear(new ActionEvent());
                    event.consume();
                } else if (event.getCode().equals(KeyCode.F6)) {
                    OnRun(new ActionEvent());
                    event.consume();
                } else if (event.getCode().equals(KeyCode.S) && event.isControlDown()) {
                    OnSave(new ActionEvent());
                    event.consume();
                } else if (event.getCode().equals(KeyCode.F) && event.isAltDown() && event.isShiftDown()) {
                    OnFormat(new ActionEvent());
                    event.consume();
                } else if (event.getCode().equals(KeyCode.F2)) {
                    OnSearchSelected(new ActionEvent());
                    event.consume();
                }
            }
        });
        node.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.F) && event.isAltDown() && event.isShiftDown()) {
                    event.consume();
                } else if (event.getCode().equals(KeyCode.S) && event.isControlDown()) {
                    event.consume();
                }
            }
        });

    }

    public void terminateCurrentProcess() {
        Thread t = new Thread() {
            public void run() {
                if (currentProcess != null) {
                    currentProcess.destroy();
                }
            }
        };
        t.start();
    }

    @FXML
    void OnClose(ActionEvent event) {
        ThreadUtil.shutdown();
        if (checkBeforeFileClose()) {
            ((Stage) (this.buttonRun.getScene().getWindow())).close();
        }
    }

    public boolean checkBeforeFileClose() {
        for (Tab tab : tabPane.getTabs()) {
            FileTab fileTab = (FileTab) tab;
            if (fileTab.isModified()) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("File Modified!");
                alert.setHeaderText(null);
                alert.setContentText("Please save before close.");
                alert.showAndWait();
                return false;
            }
        }
        return true;
    }

    @FXML
    void OnSave(ActionEvent event) {
        try {
            saveCurrentTab(false);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @FXML
    void OnSaveAs(ActionEvent event) {
        try {
            saveCurrentTab(true);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    void OnFormat(ActionEvent event) {
        FileTab tab = (FileTab) tabPane.getSelectionModel().getSelectedItem();
        if (tab == null) {
            return;
        }
        tab.reformat();
    }

    @FXML
    void OnSearch(ActionEvent event) {
        String keyword = textSearch.getText();
        new SearchWebAction(tabPaneSearchResults, keyword).execute();
    }

    @FXML
    void OnSearchSelected(ActionEvent event) {
        FileTab tab = (FileTab) tabPane.getSelectionModel().getSelectedItem();
        if (tab == null) {
            return;
        }
        String text = tab.getSelectedText();
        textSearch.setText(text);
        OnSearch(new ActionEvent());
    }

    @FXML
    void onAbout(ActionEvent event
    ) {
        final Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("jCake");
        alert.setContentText("version 1.1.0");
        alert.showAndWait();
    }

    @FXML
    void OnToJavaMode(ActionEvent event
    ) {
        FileTab tab = (FileTab) tabPane.getSelectionModel().getSelectedItem();
        if (tab != null) {
            try {
                String code = tab.getMfx().getEditor().getDocument().getText();
                String javaCode = ConversionUtil.jshToJava(code);
                FileTab newTab = new FileTab();
                newTab.getMfx().getEditor().getDocument().setText(javaCode);
                redirectKeyReleaseEvent(newTab.getMfx());
                tabPane.getTabs().add(newTab);
            } catch (IOException ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    void OnUpdate(ActionEvent event
    ) {
        try {
            File deployFolder = new File("deploy");
            ProcessBuilder pb = new ProcessBuilder(jbangExecutable.getAbsolutePath(), "updater.java");
            if (deployFolder.exists() && deployFolder.isDirectory()) {
                pb.directory(deployFolder);
            } else {
                pb.directory(new File("."));
            }
            pb.start();
        } catch (IOException ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
