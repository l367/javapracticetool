/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package yzu.imsofa.network.javapracticetool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author lendle
 */
public class ThreadUtil {
    private static ExecutorService executorService=Executors.newFixedThreadPool(10);
    public static void execute(Runnable r){
        executorService.submit(r);
    }
    public static void shutdown(){
        executorService.shutdown();
    }
}
