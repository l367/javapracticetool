/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package yzu.imsofa.network.javapracticetool.test;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class TestArrayDetector {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        String src=FileUtils.readFileToString(new File("test.jsh"), "utf-8");
//        Pattern pattern = Pattern.compile("([a-zA-Z0-9_]+)(\\[\\])+");
        Pattern pattern = Pattern.compile("[;}](\\s)*([a-zA-Z0-9_]+)(\\[\\])+[^;]+;");
        Matcher matcher = pattern.matcher(src);
        while(matcher.find()){
            System.out.println(matcher.group());
        }
    }
    
}
