String input="aabaa";
String state="s0";

for(int i=0; i<input.length(); i++){
    char c=input.charAt(i);
    if(state.equals("s0")){
        if(c=='a'){
            state="s1";
        }else if(c=='b' || c=='c'){
            state="s3";
        }else{
            throw new Exception("Error!");
        }
    }else if(state.equals("s1")){
        if(c=='a'){
            state="s2";
        }else if(c=='b' || c=='c'){
            state="s1";
        }else{
            throw new Exception("Error!");
        }
    }else if(state.equals("s2")){
        if(c=='a'){
            state="s2";
        }else if(c=='b' || c=='c'){
            state="s1";
        }else{
            throw new Exception("Error!");
        }
    }else if(state.equals("s3")){
        throw new Exception("Error!");
    }
}
if(state.equals("s2")==false){
    throw new Exception("Error!");
}