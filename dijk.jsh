//DEPS org.jgrapht:jgrapht-core:1.5.1
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.graph.builder.*;
import java.util.*;
import org.jgrapht.alg.shortestpath.*;

Graph<String, DefaultWeightedEdge> g = GraphTypeBuilder.<String, DefaultWeightedEdge>directed().weighted(true)
.edgeClass(DefaultWeightedEdge.class).buildGraph();
for (int i = 1; i <= 6; i++) {
	g.addVertex("" + i);
}
g.addEdge("1", "2");
g.addEdge("1", "3");
g.addEdge("2", "5");
g.addEdge("2", "4");
g.addEdge("3", "5");
g.addEdge("3", "4");
g.addEdge("4", "6");
g.addEdge("5", "6");
g.setEdgeWeight((DefaultWeightedEdge) g.getEdge("1", "2"), 2);
g.setEdgeWeight((DefaultWeightedEdge) g.getEdge("1", "3"), 5);
g.setEdgeWeight((DefaultWeightedEdge) g.getEdge("2", "5"), 10);
g.setEdgeWeight((DefaultWeightedEdge) g.getEdge("2", "4"), 5);
g.setEdgeWeight((DefaultWeightedEdge) g.getEdge("3", "5"), 8);
g.setEdgeWeight((DefaultWeightedEdge) g.getEdge("3", "4"), 9);
g.setEdgeWeight((DefaultWeightedEdge) g.getEdge("4", "6"), 4);
g.setEdgeWeight((DefaultWeightedEdge) g.getEdge("5", "6"), 3);
System.out.println(g);
DijkstraShortestPath<String, DefaultWeightedEdge> dijk = new DijkstraShortestPath<>(g);
GraphPath<String, DefaultWeightedEdge> path = dijk.getPath("1", "6");
System.out.println(path);
