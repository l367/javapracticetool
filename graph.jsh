//DEPS org.jgrapht:jgrapht-core:1.5.1
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.graph.builder.*;
import java.util.*;
import org.jgrapht.alg.tour.*;

Graph<String, DefaultEdge> g=
    GraphTypeBuilder.<String, DefaultEdge> undirected().edgeClass(DefaultEdge.class).buildGraph();

g.addVertex("a");
g.addVertex("b");
g.addVertex("c");
g.addVertex("d");

g.addEdge("a", "b");
g.addEdge("b", "c");
g.addEdge("a", "c");
g.addEdge("a", "d");
g.addEdge("c", "d");

Stack<String> stack=new Stack<>();
Map<String, Boolean> visited=new HashMap<>();
stack.push("a");
for(String v: g.vertexSet()){
    visited.put(v, false);
}

while(!stack.isEmpty()){
    String v=stack.pop();
    Set<DefaultEdge> edges=g.outgoingEdgesOf(v);
    for(DefaultEdge e : edges){
        String target=g.getEdgeTarget(e);
        if(visited.get(target)==false){
            System.out.println(target);
            visited.put(target, true);
            stack.push(target);
        }
    }
}

visited.put("a", true);
System.out.println("a");
