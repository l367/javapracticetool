import java.util.*;
class TreeEdge{
    private double weight=1;
    private TreeNode targetNode=null;
    public TreeEdge(double weight, TreeNode targetNode){
        this.weight=weight;
        this.targetNode=targetNode;
    }

    public double getWeight(){
        return weight;
    }

    public TreeNode getTargetNode(){
        return targetNode;
    }
}
class TreeNode {
	private Object value = null;
	private List<TreeEdge> childNodeEdges=new ArrayList<>();

	public TreeNode(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

	public void addChildNode(TreeNode childNode){
        this.childNodeEdges.add(new TreeEdge(1, childNode));
    }

    public void addChildNode(double weight, TreeNode childNode){
        this.childNodeEdges.add(new TreeEdge(weight, childNode));
    }

    public List<TreeEdge> getChildNodeEdges(){
        return childNodeEdges;
    }

    public List<TreeNode> getChildNodes(){
        List<TreeNode> childNodes=new ArrayList<>();
        for(TreeEdge edge : childNodeEdges){
            childNodes.add(edge.getTargetNode());
        }
        return childNodes;
    }

    public String toString(){
        return ""+value;
    }
}
TreeNode root = new TreeNode(50);
TreeNode node40 = new TreeNode(40);
TreeNode node70 = new TreeNode(70);
TreeNode node30 = new TreeNode(30);
TreeNode node45 = new TreeNode(45);
TreeNode node60 = new TreeNode(60);

root.addChildNode(node40);
root.addChildNode(node70);
node40.addChildNode(node30);
node40.addChildNode(node45);
node70.addChildNode(node60);

void dfs(TreeNode node){
    System.out.println(node);
    for(TreeNode child : node.getChildNodes()){
        dfs(child);
    }
}

dfs(root);